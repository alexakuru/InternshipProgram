package com.internproject.string;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by Alice on 06.09.2017.
 */

public class StringExercises {

    public static void main(String[] args) {
//        System.out.println(new StringExercises().countOfWordRepeats("aa bb aa bb cc", "aa"));
//        System.out.println(new StringExercises().removeCharFromStringUpdate("time to", 2));
        System.out.println(new StringExercises().upLastChars("hello my friend", 2));
    }

    public List<Character> stringToCharArray(String str) {
        return str.chars().mapToObj(c -> (char) c).collect(Collectors.toList());
    }

    public String listToString(List<Character> list) {
        StringBuilder arrayToString = new StringBuilder();
        list.forEach(arrayToString::append);

        return arrayToString.toString();
    }

    /**
     * Returns a string whose value is this string without the letter on the
     * index (@code int).
     * <p>
     * Creates a list of characters (@code stringToCharArray(str))
     * the (@code String). Removes element at the specified position in this list.
     * Converts list to string (@code listToString(list)).
     * </p>
     *
     * @param str   an input string
     * @param index the index of the element to be removed
     * @return a {@code String} that contains the characters without removing
     * character.
     * @deprecated This method is not optimized.
     */

    @Deprecated
    public String removeCharFromString(String str, int index) {
        List<Character> list = stringToCharArray(str);
        list.remove(index);
        System.out.println(list);
        return listToString(list);
    }

    /**
     * Returns a string whose value is this string without the letter on the
     * index (@code int).
     * <p>
     * <b>Note:</b> This method is an update version of (@code removeCharFromString(String, int)).
     * Gets a (@code char) of the index (@code int). Replaces the character with (@code "") to
     * remove this character. Returns new string.
     * </p>
     *
     * @param str   an input string
     * @param index the index of the element to be removed
     * @return a {@code String} that contains the characters without removing
     * character.
     * @see java.lang.String#replace(char, char).
     */

    public String removeCharFromStringUpdate(String str, int index) {
        return new StringBuilder(str).deleteCharAt(index-1).toString();
    }

    /**
     * Returns a string with duplicated first character at the beginning
     * and end of string.
     * <p>
     * Creates a list of characters (@code stringToCharArray(str))
     * the (@code String). Gets first element of list. Adds it at the
     * first and last positions of the list. Converts the list to string
     * (@code listToString(List)). Returns the sting.
     * </p>
     *
     * @param str an input (@code String)
     * @return a modified (@code String).
     * @deprecated this method is not optimized
     */

    @Deprecated
    public String addFirstCharToStartAndEndOfString(String str) {

        List<Character> list = stringToCharArray(str);
        Character firstLetter = list.get(0);
        list.add(0, firstLetter);
        list.add((list.size()), firstLetter);

        return listToString(list);
    }

    /**
     * Returns a string with duplicated first character at the beginning
     * and end of string.
     * <p>
     * Gets first element of (@code String) with (@code substring(int, int)).
     * Adds it at the first and last positions of the string. Returns the modified
     * string.
     * </p>
     *
     * @param str a (@code String)
     * @return a modified (@code String)
     * @see java.lang.String#substring(int, int)
     */

    public String addFirstCharToStartAndEndOfStringUpdate(String str) {
        String tmp = str.substring(0, 1);
        return tmp + str + tmp;
    }

    /**
     * Returns (@code boolean) if the second string is the beginning of second.
     * <p>
     * For instance, (@code "time".startsWith("timeout)) returns true, since
     * the first letters of (@code "timeout") consist with (@code "time").
     * </p>
     *
     * @param firstStr  a checked (@code String)
     * @param secondStr a (@code String) for check
     * @return logic (@code true) if the statement is correct, else (@code false);
     */

    public boolean startsWith(String firstStr, String secondStr) {
        return (firstStr != null) && (secondStr != null) && firstStr.startsWith(secondStr);
    }

    /**
     * Returns this string with upper case last (@code int) characters.
     * <p>
     * Gets last (@code int) elements with (@code substring(startIndex, lastIndex)),
     * modifies it (@code toUpperCase()), gets first elements of String and append them.
     * Returns the modified string.
     * </p>
     *
     * @param str           a (@code String)
     * @param num is a number of elements for upper case
     * @return a modified (@code String): first part of string + substring with
     * modified characters.
     * @see String#toUpperCase()
     */

    public String upLastChars(String str, int num) {
        return str.substring(0, str.length() - num) +
         str.substring(str.length() - num).toUpperCase();
    }

    /**
     * Return (@code true) if the (@code char) is sequence repeated, else (@code false).
     *
     * @param str a (@code String)
     * @param a   A char to find repetitions
     * @return a (code boolean)
     * @deprecated this method is not optimized
     */

    @Deprecated
    public boolean charRepeat(String str, char a) {
        char[] tmpArray = str.toCharArray();
        for (int i = 1; i < tmpArray.length; i++) {
            if ((a == tmpArray[i]) && (a == tmpArray[i - 1])) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns (@code boolean) if (@code char) is sequence repeated, else (@code false).
     * <p>
     * Creates a (@code Matcher) and check a (@code Pattern) for a match with (@code String).
     * If mather find repeats and if it is begins with the (@code char) return true, else false.
     * </p>
     *
     * @param str a (@code String)
     * @param a   is
     * @return a (@code true) if the statement is true and (@code false) if it is not.
     */

    public boolean charRepeatUpdate(String str, char a) {
        Matcher matcher = Pattern.compile("(\\w)\\1+").matcher(str);
        while (matcher.find()) {
            if (matcher.group().startsWith(String.valueOf(a))) {
                return true;
            }
        }
        return false;
    }
    /**
     * Returns counts of repeats of a string in another string.
     * <p>
     * Creates an array list with split words and find the repeats of (@code String)
     * with method (@code CollectionExercises.frequency(String, String). Return count of repeats
     * + 1 cause (@code frequency()) begins to count if a string occurs > 1.
     * </p>
     *
     * @param str1 a (@code String)
     * @param str2 a (@code String)
     * @return number of occurrences.
     */

    public int countOfRepeats(String str1, String str2) {

        List<String> listOfSplitWords = new ArrayList<>();
        Collections.addAll(listOfSplitWords, str1.split(" +"));
        return (Collections.frequency(listOfSplitWords, str2) + 1);
    }

    /**
     * Returns counts of repeats of a string in another string.
     *
     * @param str1 a (@code String)
     * @param str2 a (@code String)
     * @return number of occurrences.
     */
    public long countOfWordRepeats(String str1, String str2) {
        return Arrays.stream(str1.split("[ ,\\.]"))
                .filter(s -> s.equals(str2))
                .count();
    }

    /**
     * Returns a (@code String) with characters from even positions.
     *
     * @param str a (@code String)
     * @return a string with unpaired chars of string.
     */

    public char[] evenChars(String str) {
        int c = 0;
        char[] tmpArray = new char[(str.length() + 1) / 2];
        for (int i = 0; i < str.length(); i += 2) {
            tmpArray[c] = str.charAt(i);
            c++;
        }
        return tmpArray;
    }

    /**
     * Returns (@code char) array with blocks of duplicated chars.
     * <p>
     * Converts a (@code String) to array of chars. Checks if the two chars are the same,
     * saves first (@code int) and last (@code int) positions of string with occurred chars.
     * </p>
     *
     * @param str a (@code String)
     * @return an array with chars.
     */

    public String charRepeats(String str) {
        int startIndex = 0;
        int endIndex;
        List<String> tmpList = new ArrayList<>();
        char[] charArrays = str.toCharArray();
        for (int i = 1; i < charArrays.length; i++) {
            if (charArrays[i - 1] == charArrays[i]) {
                if (startIndex == 0) {
                    startIndex = i - 1;
                }
            } else if ((charArrays[i] != charArrays[i - 1]) && (startIndex != 0)) {
                endIndex = i;
                tmpList.add(str.substring(startIndex, endIndex));
                startIndex = 0;
            }
        }
        return tmpList.toString();
    }

    /**
     * Returns a (@code String) with blocks of duplicated chars.
     * <p>
     * Converts a (@code String) to array of chars. Checks if the two chars are the same,
     * saves first (@code int) and last (@code int) positions of string with occurred chars.
     * </p>
     *
     * @param str a (@code String)
     * @return a (@code String) with the duplicated chars.
     */

    public String charRepeat(String str) {
        int index = 0;
        String result = "";
        char[] tmpArray = str.toCharArray();
        for (char a : tmpArray) {
            int count = str.indexOf(a);
            if ((tmpArray[count - 1] == tmpArray[count]) && (tmpArray[0] != a)) {
                if (index == 1) {
                    result += tmpArray[count];
                }
                index++;
            }
        }
        return result;
    }

//    public String charRepeatsUpdate(String str){
//        Matcher matcher = Pattern.compile("(\\w)\\1+").matcher(str);
//        char[] tmpArray = str.toCharArray();
//            for(char a : tmpArray) {
//                if(matcher.find()) {
//                    System.out.println(matcher.group());
//                }
//            }
//        return ;
//    }

    /**
     * Return a (@code String) remove all repeated chars.
     * <p>
     * Adds a list of (@code Character) into LinkedHashSet to delete duplicates
     * and save char sequence. Converts list to a (@code String)
     * </p>
     *
     * @param str a (@code String)
     * @return a (@code String) with the duplicated chars.
     */

    public String withoutRepeat(String str) {
        Set<Character> linkedHashSet = new LinkedHashSet<>(stringToCharArray(str));
        StringBuilder word = new StringBuilder();
        linkedHashSet.forEach(word::append);
        return word.toString();
    }

    /**
     * Returns a (@code String) remove all repeated chars.
     *
     * @param str (@code String)
     * @return a modified string.
     * @see java.lang.String#replaceAll(String, String)
     */

    public String withoutRepeat2(String str) {
        return str.replaceAll("[a-z]\1+", "");
    }

    /**
     * Returns (@code true) if the strings are anagrams, else (@code false).
     * <p>
     * Compares lengths of two strings and returns. If it is (@code true),
     * converts Strings to list of (@code Characters) and sorts them. Compare
     * them and return (@code boolean).
     * </p>
     *
     * @param str1 a (@code String)
     * @param str2 a (@code String)
     * @return a (@code true) if strings are the same characters, else (@code false).
     */

    public boolean ifAnagram(String str1, String str2) {

        if (str1.length() != str2.length()) {
            return false;
        }
        List<Character> firstList = stringToCharArray(str1);
        List<Character> secondList = stringToCharArray(str2);
        Collections.sort(stringToCharArray(str1));
        Collections.sort(stringToCharArray(str2));
        return firstList.equals(secondList);
    }

    /**
     * Returns (@code true) if the strings are anagrams, else (@code false).
     * <p>
     * Compares lengths of two strings and returns. If it is (@code true),
     * converts Strings to array of (@code char) and sorts them. Compare
     * them and return (@code boolean).
     * </p>
     *
     * @param str1 a (@code String)
     * @param str2 a (@code String)
     * @return a (@code true) if strings are the same characters, else (@code false).
     */
    public boolean ifAnagram2(String str1, String str2) {
        if (str1.length() != str2.length()) {
            return false;
        }
        char[] str1Array = str1.toCharArray();
        char[] str2Array = str2.toCharArray();
        Arrays.sort(str1Array);
        Arrays.sort(str2Array);
        return Arrays.equals(str1Array, str2Array);
    }
}