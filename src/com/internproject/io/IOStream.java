package com.internproject.io;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

/**
 * Created by Alice on 08.09.2017.
 */
public class IOStream implements Serializable{
    public static void main(String[] args) {
        IOStream ioStream = new IOStream();
        File file = new File("C:/Users/Alice/IdeaProjects/Text.txt");
        Path path = Paths.get("C:/Users/Alice/IdeaProjects/Text.txt");
        ioStream.lineNumbers(file);
        ioStream.lineNumbers8(path);
        ioStream.wordNumbers(file);
        ioStream.fileCopyWithUpCases(file);
    }

    /**
     *
     * @param file
     */
    public void lineNumbers(File file){
        try {
            int lines = 0;
            BufferedReader reader = new BufferedReader(new FileReader(file));
            while(reader.readLine() != null){
                lines++;
            }
            System.out.println("Number of lines : " + lines);
            reader.close();
       }
       catch (FileNotFoundException ex) {
           System.out.println("File not found");
       }
       catch (IOException ex){
           ex.printStackTrace();
       }
    }

    /**
     * Prints number of lines in a file.
     * <p>
     * <b>Note: </b> It is the update version of (@code lineNumber(File)).
     * Gets number of lines in a file with methods specific for Java 8.
     * </p>
     * @param path (@code Path) is a file directory.
     */

    public void lineNumbers8(Path path) {
        try {
            System.out.println("Number of lines: " + Files.lines(path).count());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Prints number of words in the file.
     * <p>
     * Creates input characters stream (@code FileReader). The (@code
     * Scanner) reads lexicographically from stream. While loop checks
     * if scanner can read from stream. Gets next word, iterates count.
     * If next word isn't, breaks the while loop.
     * </p>
     * @param file (@code File) is file directory
     */
    public void wordNumbers(File file){
        int counter = 0;
        try {
            Scanner scanner = new Scanner(new FileReader(file));
            while (scanner.hasNext()) {
                scanner.next();
                counter++;
            }
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
        System.out.println(counter);
    }

    /**
     * Returns
     * @param original
     */
    public void fileCopyWithUpCases(File original){
        try {
            File copy = new File("C:/Users/Alice/IdeaProjects/TextCopy.txt");
            BufferedReader reader = new BufferedReader(new FileReader(original));
            BufferedWriter writer = new BufferedWriter(new FileWriter(copy));

            int c;
            while ((c = reader.read()) != -1){
                char character = (char) c;
                character = Character.toUpperCase(character);
                writer.write(character);
            }
            reader.close();
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
