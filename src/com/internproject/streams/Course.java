package com.internproject.streams;

/**
 * Created by Alice on 15.09.2017.
 */
public class Course {
    String title;
    Integer duration;
    Double score;

    public Course(String title, Integer duration, Double score) {
        this.title = title;
        this.duration = duration;
        this.score = score;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }
}
