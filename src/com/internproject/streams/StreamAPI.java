package com.internproject.streams;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Alice on 15.09.2017.
 */
public class StreamAPI {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        int i;
        for(i = -5; i < 5; i++){
            list.add(i);
        }
//        System.out.println(new StreamAPI().averagePositiveNumb(list));

        List<Course> bobCourses = new ArrayList<>();
        bobCourses.add(new Course("Math", 15, 5.2));
        bobCourses.add(new Course("English", 20, 6.7));
        bobCourses.add(new Course("Java", 30, 7.9));

        List<Course> aliceCourses = new ArrayList<>();
        aliceCourses.add(new Course("Math", 23, 8.2));
        aliceCourses.add(new Course("Franch", 12, 7.6));
        aliceCourses.add(new Course("Java", 45, 8.0));

        Student bob = new Student("Bob", 22, bobCourses);
        Student alice = new Student("Alice", 24, aliceCourses);

        List<Student> studentList = new ArrayList<>();
        studentList.add(bob);
        studentList.add(alice);

        System.out.println(new StreamAPI().averages(studentList));
    }

    public double averagePositiveNumb(List<Integer> list){
        return list
                .stream()
                .filter(x -> x > 0)
                .mapToInt(Integer::intValue)
                .average().getAsDouble();
    }

    public List studentNames(List<Student> list){
        return list
                .stream()
                .map(Student::getName)
                .collect(Collectors.toList());
    }

    public Integer maxAge(List<Student> list){
        return list
                .stream()
                .mapToInt(Student::getAge)
                .max().getAsInt();
    }

    public List courses(List<Student> list){
        return list
                .stream()
                .flatMap(e -> e.getCourses().stream())
                .map(Course::getTitle)
                .distinct()
                .collect(Collectors.toList());
    }

    public List averages(List<Student> list){
        return  list
                .stream()
                .map(s -> s.getCourses()
                .stream()
                .mapToDouble(Course::getScore)
                .average()
                .getAsDouble()).collect(Collectors.toList());
    }

}
