package com.internproject.streams;

import java.util.List;

/**
 * Created by Alice on 15.09.2017.
 */
public class Student {
    String name;
    Integer age;
    List<Course> courses;

    public Student(String name, Integer age, List<Course> courses) {
        this.name = name;
        this.age = age;
        this.courses = courses;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }
}
