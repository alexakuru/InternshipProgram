package com.internproject.hashandequals;

/**
 * Created by Alice on 15.09.2017.
 */
public class TestClass {
    private int a;
    private String b;

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public String getB() {
        return b;
    }

    public void setB(String b) {
        this.b = b;
    }
}
