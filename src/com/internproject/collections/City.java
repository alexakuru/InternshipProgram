package com.internproject.collections;

/**
 * Created by Alice on 08.09.2017.
 */
public class City {
    private String name;
    private Integer population;
    private Double area;

    public City(String name, Double area, Integer population) {
        super();
        this.name = name;
        this.area = area;
        this.population = population;
    }

    public String getName() {
        return name;
    }

    public Integer getPopulation() {
        return population;
    }

    public Double getArea() {
        return area;
    }
}
