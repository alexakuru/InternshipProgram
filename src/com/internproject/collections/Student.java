package com.internproject.collections;

/**
 * Created by Alice on 08.09.2017.
 */
public class Student {
    private String name;

    public Student(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
