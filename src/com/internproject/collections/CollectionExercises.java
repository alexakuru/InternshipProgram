package com.internproject.collections;

import java.util.*;

/**
 * Created by Alice on 08.09.2017.
 */
public class CollectionExercises {
    public static void main(String[] args) {

        CollectionExercises object = new CollectionExercises();

        Object a = null;
        Object b = null;
        Object c = "time";
        String string = "   ";
        Objects.equals(a, b);

        List<City> cityList = new ArrayList<>();
        cityList.add(new City("California", 163.696, 39_250_017));
        cityList.add(new City("Texas", 268.581, 27_862_596));
        cityList.add(new City("Florida", 65.755, 20_612_439));
        cityList.add(new City("New York", 54.555, 19_745_289));
        String[] stringArray = {"it", "is", "time", "to", "begin", "to", "time"};

//        System.out.println(Arrays.toString(object.noRepeats(stringArray)));
//        System.out.println(object.nameSort(cityList));
//        System.out.println(object.nameSortLambda(cityList));
//        System.out.println(object.populationSort(cityList));
//        System.out.println(object.areaSort(cityList));
//        object.studentMap();
    }

    /**
     * Sort list objects by name (@code String).
     * <p>
     * Gets a list of (@code City), sorts them by (@code compare()).
     * Returns a (@code String) names to be converted from list.
     * </p>
     * @param   list the (@code List) to be sorted.
     * @return  a (@code String) with sorted names.
     * @see     java.lang.String#compareTo(String)
     * @see     java.util.Collections#sort(List, Comparator)
     */

    public String nameSort(List<City> list) {
        String listOfNames = "";
        if (!list.isEmpty()) {
            list.sort(Comparator.comparing(City::getName));
            for (City a : list) {
                listOfNames += a.getName() + " ";
            }
        }
        return listOfNames;
    }

    /**
     * Sort list objects by name (@code String).
     * <p>
     * <b>Note:</b> Using lambda expression.
     * Gets a list of (@code City), sorts them by (@code compare()).
     * Returns a (@code String) names to be converted from list.
     * </p>
     * @param   list the (@code List) to be sorted.
     * @return  a (@code String) with sorted names.
     * @see     java.lang.String#compareTo(String)
     * @see     java.util.Collections#sort(List, Comparator)
     */

    public String nameSortLambda(List<City> list){

        if(!list.isEmpty())
        list.sort((c1, c2)->c1.getName().compareTo(c2.getName()));
        String listOfNames = "";
        for(City a : list){
            listOfNames += a.getName() + " ";
        }
        return listOfNames;
    }

    /**
     * Sorts list objects by population (@code Integer).
     * <p>
     * Gets a list of (@code City), sorts them by (@code compare()).
     * Returns a (@code String) populations to be converted from list.
     * </p>
     * @param   list the (@code List) to be sorted.
     * @return  a (@code String) with sorted populations.
     * @see     java.util.Collections#sort(List, Comparator)
     */

    public String populationSort(List<City> list){
        String str = "";
        if(!list.isEmpty()) {
            list.sort(Comparator.comparing(City::getPopulation));
            for (City a : list) {
                str += a.getPopulation() + "   ";
            }
        }
        return str;
    }
    /**
     * Sort list objects by name (@code String).
     * <p>
     * Gets a list of (@code City), sorts them by (@code compare()).
     * Returns a (@code String) names to be converted from list.
     * </p>
     * @param   list tje (@code List) to be sorted.
     * @return  a (@code String) with sorted names.
     * @see     java.lang.String#compareTo(String)
     * @see     java.util.Collections#sort(List, Comparator)
     */

    public String areaSort(ArrayList<City> list){
        String sortArea = "";
        if (!list.isEmpty()) {
            list.sort(Comparator.comparing(City::getArea));
            for (City a : list) {
                sortArea += a.getArea() + " ";
            }
        }
        return sortArea;
    }

    /**
     * Returns an array with (@code String) without duplicates.
     * <p>
     * Creates a set with (@code String) converted to an list with
     * method (@code Arrays.asList(String)). HashSet delete all repeats.
     * Returns an (@code String) array converted from set.
     * </p>
     * @param str (@code String)
     * @return a (@code String) array.
     * @see java.util.Arrays#asList(Object[])
     * @see java.util.List#toArray(Object[])
     */

    public String[] noRepeats(String[] str){
        HashSet<String> set = new HashSet<>(Arrays.asList(str));
        return set.toArray(new String[set.size()]);
    }

    /**
     * Returns a map with objects (@code Student).
     * <p>
     * Generates a (@code Map) with key - name of students and value - grade.
     * Returns created map.
     * </p>
     * @return a map with student names (key) and their grades (value).
     */

    public Map<String, Integer> studentMap(){
        Map<String, Integer> studentMap = new HashMap<>();
        studentMap.put(new Student("Olea").getName(), 90);
        studentMap.put(new Student("Mihai").getName(), 45);
        studentMap.put(new Student("Alisa").getName(), 56);
        studentMap.put(new Student("Mark").getName(), 75);

        keyByValue(studentMap, Collections.min(studentMap.values()));
        keyByValue(studentMap, Collections.max(studentMap.values()));
        return studentMap;
    }

    /**
     * Shows a key by value.
     * <p>
     * Prints the key which have value (@code grade).
     * </p>
     * @param map (@code Map)
     * @param grade (@code Integer)
     */

    public void keyByValue(Map<String, Integer> map, Integer grade) {
        map.keySet()
                .stream()
                .filter(o -> map.get(o).equals(grade))
                .forEach(System.out::println);
    }
}