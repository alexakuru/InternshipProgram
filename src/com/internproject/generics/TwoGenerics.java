package com.internproject.generics;

/**
 * Created by Alice on 12.09.2017.
 */
public class TwoGenerics<T, V> {
    private T obj1;
    private V obj2;

    public TwoGenerics(T obj1, V obj2) {
        this.obj1 = obj1;
        this.obj2 = obj2;
    }

    public T getObj1() {
        return obj1;
    }

    public V getObj2() {
        return obj2;
    }
    void showTypes() {
        System.out.println("Type of T is " + obj1.getClass().getName());
        System.out.println("Type of V is " + obj2.getClass().getName());
    }

    public static void main(String[] args) {
        TwoGenerics<Integer, String> twoGenerics = new TwoGenerics<>(15, "Generics");
        twoGenerics.showTypes();
        System.out.println(twoGenerics.getObj1());
        System.out.println(twoGenerics.getObj2());
    }
}
