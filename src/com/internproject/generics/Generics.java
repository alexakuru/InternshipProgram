package com.internproject.generics;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alice on 08.09.2017.
 */
public class Generics {
    public static void main(String[] args) {
        //Generics generics = new Generics();

        Pair<String, Integer> pair = new Pair<>();
        pair.setKey("Alice");
        pair.setValue(23);
        System.out.println(pair.getKey() + " " + pair.getValue());
        Pair<Object, Object> object = new Pair<>();
        object.setValue(new Integer(8));
        object.setKey("Bob");
        //  String str = (String) object.getSecond();  // Runtime error - cannot cast an Integer to String
        //  System.out.println(str);

        boolean same = Generics.compare(pair, pair);
        System.out.println(same);

        List list = new ArrayList();
        list.add("hello world");
        String s = (String) list.get(0);

    }

    public static <T, V> boolean compare(Pair<T, V> p1, Pair<T, V> p2) {
        return p1.getKey().equals(p2.getKey()) &&
                p1.getValue().equals(p2.getValue());
    }
}
