package com.internproject.generics;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Alice on 15.09.2017.
 */
public class SecondEx {
    public static void main(String[] args) {
        SecondEx secondEx = new SecondEx();
        System.out.println(secondEx.minMaxWords("tra tra tu ta tum ta tramp trump").getValue());
    }

    public Pair minMaxWords(String str){
        Pair<String, String> pair = new Pair<>();
        List<String> list = Arrays.asList(str.split(" +"));
        list.sort((s1, s2) -> (s1.length() - s2.length()));
        pair.setKey(list.get(0));
        int maxLength = list.get(list.size()-1).length();
        list.stream()
                .filter(s -> (s.length() == maxLength) && (s.length() != list.get(list.indexOf(s) - 1).length()))
                .forEach(pair::setValue);
        return pair;
    }
}
