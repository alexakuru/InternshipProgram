package com.internproject.generics;

import java.util.*;

/**
 * Created by Alice on 14.09.2017.
 */
public class ThirdEx {
    public static void main(String[] args) {
        System.out.println(new ThirdEx().charFrequency("tra ra aa tum tu me eo oh ello").getValue());
    }

    public Pair charFrequency(String str){
        Pair<String, List<String>> pair = new Pair<>();
        Map<String, Long> map = new HashMap<>();
        String tmp = str.replaceAll("\\W", "");
        String[] string = tmp.split("(?!^)");

        for(String s : string){
            map.put(s, Arrays.stream(string)
                    .filter(t -> t.equals(s))
                    .count());
        }

        int count = 0;
        for(Map.Entry<String, Long> e : map.entrySet()){
            if(e.getValue().equals(Collections.max(map.values())) && (count == 0)){
                pair.setKey(e.getKey());
                count++;
            }
        }

        List<String> list = new ArrayList<>();
        for(String s : str.split(" ")){
            if(s.contains(pair.getKey())){
                list.add(s);
            }
        }
        pair.setValue(list);
       return pair;
    }
}
