package com.internproject.generics;

import java.util.HashMap;

/**
 * Created by Alice on 14.09.2017.
 */


public class DefaultHashMap<K, V> extends HashMap<K, V> {
    private V def;

    public DefaultHashMap(V def) {
        this.def = def;
    }

    public V get(Object k){
        if(super.containsKey(k)){
            return super.get(k);
        }
        return def;
    }
}
