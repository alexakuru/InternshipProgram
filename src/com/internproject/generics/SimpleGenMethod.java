package com.internproject.generics;

/**
 * Created by Alice on 13.09.2017.
 */
/*
Java 2, v5.0 (Tiger) New Features
by Herbert Schildt
ISBN: 0072258543
Publisher: McGraw-Hill/Osborne, 2004
*/
public class SimpleGenMethod {

    // Determine if an object is in an array.
    static <T, V extends T> boolean isIn(T x, V[] y) {

        for (int i = 0; i < y.length; i++)
            if (x.equals(y[i])) return true;

        return false;
    }

    public static void main(String args[]) {

        // Use isIn() on Integers.
        Integer nums[] = {1, 2, 3, 4, 5};

        if (isIn(2, nums))
            System.out.println("2 is in nums");

        if (!isIn(7, nums))
            System.out.println("7 is not in nums");

        System.out.println();

        // Use isIn() on Strings.
        String strs[] = {"one", "two", "three",
                "four", "five"};

        if (isIn("two", strs))
            System.out.println("two is in strs");

        if (!isIn("seven", strs))
            System.out.println("seven is not in strs");

        // Opps! Won't compile! Types must be compatible.
        if(!isIn("two", nums))
            System.out.println("two is not in strs");

        isIn("yrdy", new Pair[]{});

        //My test
        Integer[] i = {10, 20, 30, 40};
        Object[] obj = {};
        Number v = 10;
//
        if (isIn(v, i)) {
            System.out.println("10 is not i"); //It works
        }
    }
}
