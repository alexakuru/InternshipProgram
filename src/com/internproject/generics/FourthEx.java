package com.internproject.generics;

import java.util.*;

/**
 * Created by Alice on 14.09.2017.
 */
public class FourthEx {
    public static void main(String[] args) {
        String[] str =  {"time", "time", "to", "say", "to"};
        FourthEx fourthEx = new FourthEx();
        System.out.println(fourthEx.varArgToSet(str));
        System.out.println(fourthEx.varArgToMap(str));
    }

    @SafeVarargs
    final public <T> Map<T, Long> varArgToMap(T ... vararg){
        Map<T, Long> map = new HashMap<>();
        for(T t : vararg){
            map.put(t,
                    Arrays.stream(vararg)
                    .filter(s -> s.equals(t))
                    .count());
        }
        return map;
    }

    //The compiler is corrected me.
    @SafeVarargs
    final public <T> Set<T> varArgToSet(T ... vararg) {
        Set<T> set = new HashSet<>();
        set.addAll(Arrays.asList(vararg));
        return set;
    }
}
