package com.internproject.generics;

/**
 * Created by Alice on 13.09.2017.
 */
public class GenCons {
        private Object val;

        GenCons(Object arg) {
            val = arg;
            System.out.println(val.getClass().getName());;
        }
    }

class GenConsDemo {
    public static void main(String args[]) {

        GenCons test = new GenCons(100);
        GenCons test2 = new GenCons(123.5F);
    }
}